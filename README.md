# Running services in the HPC cluster on demand. A MongoDB example.

With this example we are going to create a MongoDB Singularity image that then we will use to run in the cluster and do some computation.

## Why should we use Singularity?

Singularity is just another container tool that brings containers and reproducibility to scientific computing. Using Singularity containers, developers can work in reproducible environments of their choosing and design, and these complete environments can easily be copied and executed on other platforms.

- Easy way to pack our software stack
- Plenty of Docker/Singularity images already available (https://hub.docker.com/u/biocontainers/)
- Great community support
- Make it reproducible and portable, so other colleagues may use it
- Existing software already support Singularity, like conda
- MPI support, GPU support
- And many more (https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0177459)


## Pre-requisites

You will need:

- `sudo` rights over `singularity` command to build your own images, but you only need them on the machines (physical or VM) where you want to build the images, not on the hosts where you just want to run your containers.

- Access to the EMBL HPC cluster (or any other with singularity installed).

## Getting started with images

Images can be build from Singularity recipes like the file we have in this repo (named Singularity), from Singularity Hub, Docker hub. (Remember you need to have sudo rights for this, **but not for running them**)

To build it from Singularity hub:

      sudo singularity build lolcow.simg shub://GodloveD/lolcow

If you want to build it from Docker hub:

      sudo singularity build lolcow.simg docker://godlovedc/lolcow


## Local build from a recipe

**DO NOT DO IT FROM A SHARED FILESYSTEMS**

First, clone the repo.

      git clone https://git.embl.de/grp-bio-it/singularity-service-example.git
      cd singularity-service-example
      
Take a look at `Singularity` recipe

     Bootstrap: docker
     From: mongo:4.0.6
     
     %startscript
     
     /usr/bin/mongod --config /etc/mongo/mongod.conf
     
     %post
     
     chmod 777 /data/db


Then, on your local machine, create an image for the mongo database providing the Singularity file (Note that you will require sudo rights for the singularity command).

     sudo singularity build mongo_4.0.6.img Singularity

Then we need to change permissions of the image to your user:

     sudo chown <user>:<group> mongo_4.0.6.img
    
Copy it to the cluster where you have this repo cloned

     scp mongo_4.0.6.img login:/scratch/moscardo/singularity-service-example
### Sections of a recipe

#### %startscript (only used by Singularity instances)
- Commands that we want our instance to execute 

#### %setup
- Runs commands outside of the container at start of the bootstrap process
- Runs before %post section

#### %post
- Runs once inside the container during bootstrap process
- Software installation (apt-get install mongodb-server)

#### %files
- Copy files from outside of the image to the inside of it
- Pairs of <source> <destination>
- Runs after %post section

#### %runscript
- Define custom runscript
- Command line parameter parsing etc...

#### %environment
- Define environment variables inside container

#### %labels
- Define custom labels/metadata
- $ singularity inspect <image>

#### %help
- Add help for the image
- $ singularity help ubuntu.img


## Running the service as instance

This is the recommended way to run permanent service (as long as the job runs) and get detached from the terminal. It provides you mechanisms to start and stop the instance, execute inside the instance...

For this example we are going to run a MongoDB instance, insert some data, query the DDBB, and get the results back.

If we don't have the DDBB created, we'll create it first in the login node. Go to <some-directory> and `git pull` this project.

    git clone https://git.embl.de/grp-bio-it/singularity-service-example.git

Now we are ready to create the database:

    singularity instance start  -B $PWD/data:/var/lib/mongo -B $PWD/mongoconf:/etc/mongo -B $PWD/log:/var/log/mongodb  /scratch/moscardo/singularity-service-example/mongo_4.0.6.img mongo

If you take a look at the the directories `data`, `log` and `mongoconf`, you will see they contain files now. With `-B` we just mount those local directories into the container to get logs and data out of it

To check the instances running:

    [moscardo@login singularity-service-example]$ singularity instance list
    DAEMON NAME      PID      CONTAINER IMAGE
    mongo            35473    /scratch/moscardo/singularity-service-example/mongo_4.0.6.img


We can see that it just runs as a normal process in the system

    [moscardo@login singularity-service-example]$ ps aux | grep mongod
    moscardo 35477  3.8  0.6 1086512 53560 ?       Sl   14:27   0:02 /usr/bin/mongod --config /etc/mongo/mongod.conf


We can just gracefully shutdown the DDBB.

    15:34 $ singularity instance stop mongo
    Stopping mongo instance of /home/xemacs/sourcecode/mongo/mongo_4.0.6.img (PID=40273)    

So the next steps will:

- Copy the DDBB to a node in the cluster on tmpfs
- Start an instance with all the data mounted as volume
- Insert some data
- Query and print the data to a file
- Copy back to the network filesystem.

Here is the sbatch script that we will be running.

    #!/bin/bash
    
    #SBATCH --mem 8G
    #SBATCH -t 10:00
    #SBATCH --gres=tmp:1G
    
    # Make sure the directories exists
    MONGOPATH=/scratch/sing-training/moscardo/singularity-service-example
    
    echo "Copying DDBB"
    time cp -ra $MONGOPATH/data $TMPDIR
    
    # --verbose flag can be added to Singularity for debugging purposes.
    echo "Running database"
    time singularity instance.start -B $MONGOPATH:$MONGOPATH -B $TMPDIR/data:/var/lib/mongo -B $MONGOPATH/mongoconf:/etc/mongo/ $MONGOPATH/mongo_4.0.6.img  mongo
    
    # Giving some time to start up the DDBB, useful when not shutted down properly 
    sleep 30
    
    # 
    #Do your computation here
    # 
    
    # Insert results data into the DDBB
    time singularity  exec instance://mongo mongo 127.0.0.1/testddbb --eval 'var document = [{name  : "John",position : "Teacher",}, {name  : "Marie",position : "Doctor",}];db.MyCollection.insert(document);'
    
    # Getting results from a query
    echo "Query ...."
    time singularity  exec instance://mongo  mongoexport -d testddbb --port 27017 -c MyCollection --query '{"position":{"$eq": "Teacher" }}' --out $TMPDIR/test.out --type csv --fields name,position
    
    sleep 5
    echo "Copying in memmory results to FS"
    time cp $TMPDIR/test.out $MONGOPATH
    
    sleep 5
    echo "Stopping DDBB instance"
    time singularity instance.stop mongo
    
    echo "Copying DDBB to FS"
    time cp -ra $TMPDIR/data $MONGOPATH/data-mod

**Important to know when dealing with MongoDB** Mongo needs indexes to perform quite well, and those may take up a lot of storage that we are actually moving to tmpfs, so just keep it in mind when asking for resources


## Apps
What if you want to build a single container with two or three different apps that each have their own runscripts and custom environments? In some circumstances, it may be redundant to build different containers for each app with almost equivalent dependencies, in those cases we'll build a single image but configured to be called and used with different apps.

## Documentation
This is just an introductory manual to Singularity, but there is a world out there, just check their documentation and more resources available.

- https://www.sylabs.io/guides/2.6/user-guide/
- http://prace.it4i.cz/sites/prace.it4i.cz/files/files/perftools-hrabal-singularity.pdf
- https://www.westgrid.ca/files/WG%20singularity%20Nov21.pdf
- https://slurm.schedmd.com/SLUG17/SLUG_Bull_Singularity.pdf

## Support from them
I would like to mention that the support from their side is just great, as soon as you hit a bug or have any issue, you report it to them and they are willing to help you and fix it, interaction with them very easy. 

## Something to add....
We may want to think about adding a user task epilog just to make sure that we stop the instance in case the query reaches Time Limit.
